window.onload = function() {
    Plotly.d3.csv("data-text.csv", MAIN);
    DOMs();
};


window.onresize = function() {
    Plotly.d3.csv("data-text.csv", MAIN);
};


function MAIN(error, data) {

    backtrackChoropleth(error, data);
    backtrackRegion(error, data);
    backtrackGender(error,data);
    backtrackScatter(error, data);


    document.getElementById("ButtonChoropleth").onclick = function() {
        backtrackChoropleth(error, data);
        backtrackRegion(error, data);
        backtrackGender(error, data);
    };
    document.getElementById("RegionBar").onchange = function() {
        backtrackRegion(error, data);
        backtrackGender(error, data);
    };
    document.getElementById("DOMin1").onchange = function() {
        backtrackScatter(error, data);
    };
    document.getElementById("DOMin2").onchange = function() {
        backtrackScatter(error, data);
    };
    document.getElementById("SexScatter").onchange = function() {
        backtrackScatter(error, data);
    };
    document.getElementById("ButtonTable").onclick = function() {
        backtrackTable(error, data);
        document.getElementById("JS_Table").style.height = '500px';
        document.getElementById("JS_Table").style.overflow = 'auto';
    };
    document.getElementById("HideTable").onclick = function() {
       document.getElementById("OriginalData").style.display = 'none';
       document.getElementById("JS_Table").style.height = '0';
    };

}


function backtrackTable(error, data) {

    let content = "<table id='OriginalData' class='dataTable'>";

    content += '<tr>' +
        '<th>' + "Year" + '</th>' +
        '<th>' + "WHO Region" + '</th>' +
        '<th>' + "Country" + '</th>' +
        '<th>' + "Sex" + '</th>' +
        '<th>' + "Display Value" + '</th>' +
        '</tr>';

    data.sort( (a, b)=>{
        if( a.Country === b.Country )
            return b.Year - a.Year;
        else
            return a.Country.localeCompare(b.Country);
    } );

    for(let i=0; i<data.length; i++){

        if(data[i]["Indicator"]==="Healthy life expectancy (HALE) at birth (years)"){
            content += '<tr>' +
                '<td>' + data[i]["Year"] + '</td>' +
                '<td>' + data[i]["WHO region"] + '</td>' +
                '<td>' + data[i]["Country"] + '</td>' +
                '<td>' + data[i]["Sex"] + '</td>' +
                '<td>' + data[i]["Display Value"] + '</td>' +
                '</tr>';
        }

    }
    content += "</table>";

    $('#JS_Table').append(content);

}


function backtrackChoropleth(error, data) {

    if(error) return console.warn(error);


    let sex = document.getElementById("SexChoropleth").value;
    let year = document.getElementById("YearChoropleth").value;

    let o = fill(data, 0, sex, year, null, null);

    let traces = {
        type: 'choropleth',
        locationmode: 'country names',
        locations: o.country,
        z: o.value,
        zmin: 30,
        zmax: 80,
        text: o.region,
        hoverinfo: 'location + z + text',
        colorbar: {
            title: 'Healthy life expectancy',
            thickness: 20
        },
        colorscale: o.scale,
        reversescale: o.reverse,
        marker: {
            line:{
                color: 'black',
                width: 0.4
            }
        }
    };

    let layout = {
        geo:{
            scope: 'world',
            projection: {  type: 'robinson' },
            showlakes: false,
            lakecolor: 'azure',
            shownocean: true,
            oceancolor: 'azure'
        },
        margin : {
            t : 25, r : 25, l : 25, b : 25
        },
        automargin: true
    };

    Plotly.newPlot("PLOTLY_choropleth", [traces], layout);


    let b;
    (sex==="Male") ? b="genere Maschile" :
        (sex==="Female") ? b="genere Femminile" :
            b="entrambi i generi";

    document.getElementById("textChoropleth").innerHTML = "Andamento globale " +
        "dell'indice HALE, nell'anno " + year + ", per il " + b ;


    // _______________________________________


    let o2 = fill(o, 2, sex, null, null);

    let traces2 = {
        type:'bar',
        orientation: 'h',
        x: o2.average,
        y: o2.region,
        marker: { color: o2.myColor }
    };

    let layout2 = {
        margin : {
            t : 25, r : 25, l : 200, b : 25
        },
        hovermode: 'closest',
        height: 250,
        autosize: true
    };

    Plotly.newPlot("PLOTLY_bar0", [traces2], layout2);


    (sex==="Male") ? b="genere Maschile" :
        (sex==="Female") ? b="genere Femminile" :
            b="entrambi i generi";

    document.getElementById("regionBarText").innerHTML = "Andamento per regioni " +
        "dell'indice HALE, nell'anno " + year + ", per il " + b ;

}

function backtrackRegion(error, data) {

    if(error) return console.warn(error);

    function unpack(data, key) {
        return data.map( function (x) {
            return x[key];
        } );
    }

    let region = document.getElementById("RegionBar").value;
    let sex = document.getElementById("SexChoropleth").value;
    let year = document.getElementById("YearChoropleth").value;

    let o1 = fill(data, 1, region, sex, year);

    let traces = {
        type:'bar',
        orientation: 'v',
        x: unpack(o1, 'code'),
        y: unpack(o1, 'value'),
        text: unpack(o1, 'country'),
        marker: { color: o1[0].myColor }
    };

    let layout = {
        margin : {
            t : 25, r : 25, l : 25, b : 25
        },
        showlegend: false,
        height: 300
    };

    Plotly.newPlot("PLOTLY_bar1", [traces], layout);


    let b;
    (sex==="Male") ? b="genere Maschile" :
        (sex==="Female") ? b="genere Femminile" :
            b="entrambi i generi";

    document.getElementById("singleRegionText").innerHTML = "Andamento dei singoli " +
        "stati nella regione " + region + " dell'indice HALE, nell'anno "
        + year + ", per il " + b ;

}


function backtrackGender(error,data){

    if(error) return console.warn(error);

    function unpack(data, key) {
        return data.map( function (x) {
            return x[key];
        } );
    }

    let year = document.getElementById("YearChoropleth").value;
    let region = document.getElementById("RegionBar").value;

    let o3 = fill(data, 3, year, region, null);

    let traces = [{
        name: 'Male',
        type: 'bar',
        orientation: 'h',
        x: unpack(o3, 'male'),
        y: unpack(o3, 'code'),
        text: unpack(o3, 'country'),
    },
        {
            name: 'Female',
            type: 'bar',
            orientation: 'h',
            x: unpack(o3, 'female'),
            y: unpack(o3, 'code'),
            text: unpack(o3, 'country'),
            xaxis: "x2"
        }];

    let h=0;
        if(region=== 'South-East Asia')
            h= o3.length*40;
        else if( region === 'Eastern Mediterranean'|| region === 'Western Pacific')
            h = o3.length*30;
        else
            h = o3.length*25;

    let layout = {
        height: h,
        //title: 'WHO2',
        xaxis: { domain: [0, 0.46],
            autorange: 'reversed' },
        xaxis2: { domain: [0.54, 1] },
        yaxis : { position: 0.52 },
        margin : {
            t : 25, r : 25, l : 25, b : 25
        },
        showlegend: true,
        legend:{ y:1,x:0.5,
            yanchor:"bottom",
            xanchor:"center",
            orientation:"h"
        },
        bargap: 15
    };

    Plotly.newPlot("PLOTLY_dualBar", traces, layout);


    document.getElementById("LGBText").innerHTML = "Andamento dei singoli " +
        "stati nella regione " + region + " dell'indice HALE, nell'anno "
        + year;

}


function backtrackScatter(error, data){

    if(error) return console.warn(error);

    let country1 = document.getElementById("DOMin1").value;
    let country2 = document.getElementById("DOMin2").value;
    let sex = document.getElementById("SexScatter").value;

    let o4 = fill(data, 4, sex, country1, country2);


    let traces = [{
        x: o4.year,
        y: o4.country1,
        type: 'scatter',
        mode: 'line + point',
        name: country1,
        line: { shape: 'spline' }
    }, {
        x: o4.year,
        y: o4.country2,
        type: 'scatter',
        mode: 'line + point',
        name: country2,
        line: { shape: 'spline' }
    }];

    let layout = {
        margin : {
            t : 25, r : 25, l : 25, b : 25
        },
        showlegend: false,
    };

    Plotly.newPlot("PLOTLY_bar2", traces, layout);


    let b;
    (sex==="Male") ? b="genere Maschile" :
        (sex==="Female") ? b="genere Femminile" :
            b="entrambi i generi";

    document.getElementById("multipleStateText").innerHTML = "Confronto nel tempo" +
        " tra <strong>" + country1 + "</strong> e <strong>" + country2 + "</strong>, per " + b ;

}


function fill(data, type, str1, str2, str3) {

    switch (type) {
        case 0:

            let o = {
                country: [],
                value: [],
                region: [],
                scale: [],
                reverse: true
            };

            for(let i=0;i<data.length;i++){
                if( data[i].Indicator === "Healthy life expectancy (HALE) at birth (years)" &&
                    data[i].Sex === str1 &&
                    data[i].Year === str2 ){

                    o.country.push( data[i].Country );
                    o.value.push( data[i]["Display Value"] );
                    o.region.push( data[i]["WHO region"] );

                }
            }

            (str1==="Male") ? o.scale='YIGnBu' :
                (str1==="Female") ? o.scale='Reds' :
                    o.scale='Greens';

            if(str1==="Female") o.reverse = false;

            return o;

        case 1:

            let o1 = [];
            let color;

            (str2==="Male") ? color='#195995' :
                (str2==="Female") ? color='#B73523' :
                    color="#739C4A";

            for(let i=0;i<data.length;i++){
                if( data[i].Indicator === "Healthy life expectancy (HALE) at birth (years)" &&
                    data[i].Sex === str2 &&
                    data[i].Year === str3 &&
                    data[i]["WHO region"]===str1 ){

                    o1.push( {
                        country: data[i].Country,
                        code: getCountry(0, 2, data[i].Country),
                        value: data[i]["Display Value"],
                        myColor: color
                    } );

                }
            }

            o1.sort( (a, b)=> b.value - a.value );

            return o1;

        case 2:

            let o2 = {
                region: ["Americas", "Europe", "Africa", "Eastern Mediterranean", "South-East Asia", "Western Pacific"],
                average: [0, 0, 0, 0, 0, 0],
                nCountry: [0, 0, 0, 0, 0, 0],
                myColor: null
            };

            for(let i=0;i<data.country.length;i++){
                if( data.region[i]==="Americas" ){ o2.average[0] += +data.value[i]; o2.nCountry[0]++; }
                else if (data.region[i]==="Europe") { o2.average[1] += +data.value[i]; o2.nCountry[1]++; }
                else if (data.region[i]==="Africa") { o2.average[2] += +data.value[i]; o2.nCountry[2]++; }
                else if (data.region[i]==="Eastern Mediterranean") { o2.average[3] += +data.value[i]; o2.nCountry[3]++; }
                else if (data.region[i]==="South-East Asia") { o2.average[4] += +data.value[i]; o2.nCountry[4]++; }
                else { o2.average[5] += +data.value[i]; o2.nCountry[5]++; }

            }

            for(let i=0;i<6;i++){
                o2.average[i] = o2.average[i]/o2.nCountry[i];
            }

            (str1==="Male") ? o2.myColor='#195995' :
                (str1==="Female") ? o2.myColor='#B73523' :
                    o2.myColor="#739C4A";

            let swapped;
            do {
                swapped = false;
                for (let i=0; i<6-1; i++) {
                    if (o2.average[i] > o2.average[i+1]) {
                        let temp1 = o2.region[i];
                        let temp2 = o2.average[i];
                        let temp3 = o2.nCountry[i];

                        o2.region[i] = o2.region[i+1];
                        o2.region[i+1] = temp1;
                        o2.average[i] = o2.average[i+1];
                        o2.average[i+1] = temp2;
                        o2.nCountry[i] = o2.nCountry[i+1];
                        o2.nCountry[i+1] = temp3;

                        swapped = true;
                    }
                }
            } while (swapped);

            return  o2;

        case 3:
            let o3 = [];
            let newData = [];
            let m, f, j = 0;

            for(let i=0;i<data.length;i++){
                if( data[i].Indicator !== "Healthy life expectancy (HALE) at age 60 (years)" &&
                    data[i].Year === str1  &&
                    data[i].Sex !== "Both sexes" &&
                    data[i]["WHO region"] === str2 ) {
                    newData.push(data[i]);
                }
            }

            newData.sort( (a, b)=> -b.Country.localeCompare(a.Country) );
            for(let i=0;i<newData.length;i++){

                if( newData[i].Sex === 'Male' ) { m = +newData[i]["Display Value"]; }
                else  { f = +newData[i]["Display Value"]; }

                if(j===1){
                    o3.push( {
                        country: newData[i].Country,
                        male: m,
                        female: f,
                        code: getCountry(0, 2, newData[i].Country)
                    } );

                    j--;
                }else{
                    j++;
                }

            }

            o3.sort( (a, b)=> b.code.localeCompare(a.code) );

            return o3;

        case 4:

            let o4 = {
              year: [],
              country1: [],
              country2: []
            };

            let newData1 = [];
            let newData2 = [];

            for(let i=0;i<data.length;i++){
                if( data[i].Indicator === "Healthy life expectancy (HALE) at birth (years)" &&
                    data[i].Sex === str1 ) {
                    if( data[i].Country===str2)
                        newData1.push(data[i]);

                    else if( data[i].Country===str3)
                        newData2.push(data[i]);
                }

            }
            newData1.sort((a,b)=> a.Year - b.Year);
            newData2.sort((a,b)=> a.Year - b.Year);

            for(let i=0; i<newData1.length; i++){
                o4.year.push(newData1[i].Year);
                o4.country1.push(newData1[i]["Display Value"]);
                o4.country2.push(newData2[i]["Display Value"]);
            }

            return o4;

        default:
            window.alert("An error occurred!\ntry again later");
            break;
    }

}


/* _____________________________________________________________________________________________ */


function getCountry(i, n, c) {

    let nameCode = [
        {
            "name": "Afghanistan",
            "code": "AFG"
        },
        {
            "name": "Albania",
            "code": "ALB"
        },
        {
            "name": "Algeria",
            "code": "DZA"
        },
        {
            "name": "Angola",
            "code": "AGO"
        },
        {
            "name": "Antigua and Barbuda",
            "code": "ATG"
        },
        {
            "name": "Argentina",
            "code": "ARG"
        },
        {
            "name": "Armenia",
            "code": "ARM"
        },
        {
            "name": "Australia",
            "code": "AUS"
        },
        {
            "name": "Austria",
            "code": "AUT"
        },
        {
            "name": "Azerbaijan",
            "code": "AZE"
        },
        {
            "name": "Bahamas",
            "code": "BHS"
        },
        {
            "name": "Bahrain",
            "code": "BHR"
        },
        {
            "name": "Bangladesh",
            "code": "BGD"
        },
        {
            "name": "Barbados",
            "code": "BRB"
        },
        {
            "name": "Belarus",
            "code": "BLR"
        },
        {
            "name": "Belgium",
            "code": "BEL"
        },
        {
            "name": "Belize",
            "code": "BLZ"
        },
        {
            "name": "Benin",
            "code": "BEN"
        },
        {
            "name": "Bhutan",
            "code": "BTN"
        },
        {
            "name": "Bolivia (Plurinational State of)",
            "code": "BOL"
        },
        {
            "name": "Bosnia and Herzegovina",
            "code": "BIH"
        },
        {
            "name": "Botswana",
            "code": "BWA"
        },
        {
            "name": "Brazil",
            "code": "BRA"
        },
        {
            "name": "Brunei Darussalam",
            "code": "BRN"
        },
        {
            "name": "Bulgaria",
            "code": "BGR"
        },
        {
            "name": "Burkina Faso",
            "code": "BFA"
        },
        {
            "name": "Burundi",
            "code": "BDI"
        },
        {
            "name": "Cabo Verde",
            "code": "CPV"
        },
        {
            "name": "Cambodia",
            "code": "KHM"
        },
        {
            "name": "Cameroon",
            "code": "CMR"
        },
        {
            "name": "Canada",
            "code": "CAN"
        },
        {
            "name": "Central African Republic",
            "code": "CAF"
        },
        {
            "name": "Chad",
            "code": "TCD"
        },
        {
            "name": "Chile",
            "code": "CHL"
        },
        {
            "name": "China",
            "code": "CHN"
        },
        {
            "name": "Colombia",
            "code": "COL"
        },
        {
            "name": "Comoros",
            "code": "COM"
        },
        {
            "name": "Congo",
            "code": "COG"
        },
        {
            "name": "Costa Rica",
            "code": "CRI"
        },
        {
            "name": "Côte d'Ivoire",
            "code": "CIV"
        },
        {
            "name": "Croatia",
            "code": "HRV"
        },
        {
            "name": "Cuba",
            "code": "CUB"
        },
        {
            "name": "Cyprus",
            "code": "CYP"
        },
        {
            "name": "Czechia",
            "code": "CZE"
        },
        {
            "name": "Democratic People's Republic of Korea",
            "code": "PRK"
        },
        {
            "name": "Democratic Republic of the Congo",
            "code": "COD"
        },
        {
            "name": "Denmark",
            "code": "DNK"
        },
        {
            "name": "Djibouti",
            "code": "DJI"
        },
        {
            "name": "Dominican Republic",
            "code": "DOM"
        },
        {
            "name": "Ecuador",
            "code": "ECU"
        },
        {
            "name": "Egypt",
            "code": "EGY"
        },
        {
            "name": "El Salvador",
            "code": "SLV"
        },
        {
            "name": "Equatorial Guinea",
            "code": "GNQ"
        },
        {
            "name": "Eritrea",
            "code": "ERI"
        },
        {
            "name": "Estonia",
            "code": "EST"
        },
        {
            "name": "Eswatini",
            "code": "SWZ"
        },
        {
            "name": "Ethiopia",
            "code": "ETH"
        },
        {
            "name": "Fiji",
            "code": "FJI"
        },
        {
            "name": "Finland",
            "code": "FIN"
        },
        {
            "name": "France",
            "code": "FRA"
        },
        {
            "name": "Gabon",
            "code": "GAB"
        },
        {
            "name": "Gambia",
            "code": "GMB"
        },
        {
            "name": "Georgia",
            "code": "GEO"
        },
        {
            "name": "Germany",
            "code": "DEU"
        },
        {
            "name": "Ghana",
            "code": "GHA"
        },
        {
            "name": "Greece",
            "code": "GRC"
        },
        {
            "name": "Grenada",
            "code": "GRD"
        },
        {
            "name": "Guatemala",
            "code": "GTM"
        },
        {
            "name": "Guinea",
            "code": "GIN"
        },
        {
            "name": "Guinea-Bissau",
            "code": "GNB"
        },
        {
            "name": "Guyana",
            "code": "GUY"
        },
        {
            "name": "Haiti",
            "code": "HTI"
        },
        {
            "name": "Honduras",
            "code": "HND"
        },
        {
            "name": "Hungary",
            "code": "HUN"
        },
        {
            "name": "Iceland",
            "code": "ISL"
        },
        {
            "name": "India",
            "code": "IND"
        },
        {
            "name": "Indonesia",
            "code": "IDN"
        },
        {
            "name": "Iran (Islamic Republic of)",
            "code": "IRN"
        },
        {
            "name": "Iraq",
            "code": "IRQ"
        },
        {
            "name": "Ireland",
            "code": "IRL"
        },
        {
            "name": "Israel",
            "code": "ISR"
        },
        {
            "name": "Italy",
            "code": "ITA"
        },
        {
            "name": "Jamaica",
            "code": "JAM"
        },
        {
            "name": "Japan",
            "code": "JPN"
        },
        {
            "name": "Jordan",
            "code": "JOR"
        },
        {
            "name": "Kazakhstan",
            "code": "KAZ"
        },
        {
            "name": "Kenya",
            "code": "KEN"
        },
        {
            "name": "Kiribati",
            "code": "KIR"
        },
        {
            "name": "Kuwait",
            "code": "KWT"
        },
        {
            "name": "Kyrgyzstan",
            "code": "KGZ"
        },
        {
            "name": "Lao People's Democratic Republic",
            "code": "LAO"
        },
        {
            "name": "Latvia",
            "code": "LVA"
        },
        {
            "name": "Lebanon",
            "code": "LBN"
        },
        {
            "name": "Lesotho",
            "code": "LSO"
        },
        {
            "name": "Liberia",
            "code": "LBR"
        },
        {
            "name": "Libya",
            "code": "LBY"
        },
        {
            "name": "Lithuania",
            "code": "LTU"
        },
        {
            "name": "Luxembourg",
            "code": "LUX"
        },
        {
            "name": "Madagascar",
            "code": "MDG"
        },
        {
            "name": "Malawi",
            "code": "MWI"
        },
        {
            "name": "Malaysia",
            "code": "MYS"
        },
        {
            "name": "Maldives",
            "code": "MDV"
        },
        {
            "name": "Mali",
            "code": "MLI"
        },
        {
            "name": "Malta",
            "code": "MLT"
        },
        {
            "name": "Mauritania",
            "code": "MRT"
        },
        {
            "name": "Mauritius",
            "code": "MUS"
        },
        {
            "name": "Mexico",
            "code": "MEX"
        },
        {
            "name": "Micronesia (Federated States of)",
            "code": "FSM"
        },
        {
            "name": "Mongolia",
            "code": "MNG"
        },
        {
            "name": "Montenegro",
            "code": "MNE"
        },
        {
            "name": "Morocco",
            "code": "MAR"
        },
        {
            "name": "Mozambique",
            "code": "MOZ"
        },
        {
            "name": "Myanmar",
            "code": "MMR"
        },
        {
            "name": "Namibia",
            "code": "NAM"
        },
        {
            "name": "Nepal",
            "code": "NPL"
        },
        {
            "name": "Netherlands",
            "code": "NLD"
        },
        {
            "name": "New Zealand",
            "code": "NZL"
        },
        {
            "name": "Nicaragua",
            "code": "NIC"
        },
        {
            "name": "Niger",
            "code": "NER"
        },
        {
            "name": "Nigeria",
            "code": "NGA"
        },
        {
            "name": "Norway",
            "code": "NOR"
        },
        {
            "name": "Oman",
            "code": "OMN"
        },
        {
            "name": "Pakistan",
            "code": "PAK"
        },
        {
            "name": "Panama",
            "code": "PAN"
        },
        {
            "name": "Papua New Guinea",
            "code": "PNG"
        },
        {
            "name": "Paraguay",
            "code": "PRY"
        },
        {
            "name": "Peru",
            "code": "PER"
        },
        {
            "name": "Philippines",
            "code": "PHL"
        },
        {
            "name": "Poland",
            "code": "POL"
        },
        {
            "name": "Portugal",
            "code": "PRT"
        },
        {
            "name": "Qatar",
            "code": "QAT"
        },
        {
            "name": "Republic of Korea",
            "code": "KOR"
        },
        {
            "name": "Republic of Moldova",
            "code": "MDA"
        },
        {
            "name": "Republic of North Macedonia",
            "code": "MKD"
        },
        {
            "name": "Romania",
            "code": "ROU"
        },
        {
            "name": "Russian Federation",
            "code": "RUS"
        },
        {
            "name": "Rwanda",
            "code": "RWA"
        },
        {
            "name": "Rwanda",
            "code": "RWA"
        },
        {
            "name": "Saint Lucia",
            "code": "LCA"
        },
        {
            "name": "Saint Vincent and the Grenadines",
            "code": "VCT"
        },
        {
            "name": "Samoa",
            "code": "WSM"
        },
        {
            "name": "Sao Tome and Principe",
            "code": "STP"
        },
        {
            "name": "Saudi Arabia",
            "code": "SAU"
        },
        {
            "name": "Senegal",
            "code": "SEN"
        },
        {
            "name": "Serbia",
            "code": "SRB"
        },
        {
            "name": "Seychelles",
            "code": "SYC"
        },
        {
            "name": "Sierra Leone",
            "code": "SLE"
        },
        {
            "name": "Singapore",
            "code": "SGP"
        },
        {
            "name": "Slovakia",
            "code": "SVK"
        },
        {
            "name": "Slovenia",
            "code": "SVN"
        },
        {
            "name": "Solomon Islands",
            "code": "SLB"
        },
        {
            "name": "Somalia",
            "code": "SOM"
        },
        {
            "name": "South Africa",
            "code": "ZAF"
        },
        {
            "name": "South Sudan",
            "code": "SSD"
        },
        {
            "name": "Spain",
            "code": "ESP"
        },
        {
            "name": "Sri Lanka",
            "code": "LKA"
        },
        {
            "name": "Sudan",
            "code": "SDN"
        },
        {
            "name": "Suriname",
            "code": "SUR"
        },
        {
            "name": "Sweden",
            "code": "SWE"
        },
        {
            "name": "Switzerland",
            "code": "CHE"
        },
        {
            "name": "Syrian Arab Republic",
            "code": "SYR"
        },
        {
            "name": "Tajikistan",
            "code": "TJK"
        },
        {
            "name": "Thailand",
            "code": "THA"
        },
        {
            "name": "Timor-Leste",
            "code": "TLS"
        },
        {
            "name": "Togo",
            "code": "TGO"
        },
        {
            "name": "Tonga",
            "code": "TON"
        },
        {
            "name": "Trinidad and Tobago",
            "code": "TTO"
        },
        {
            "name": "Tunisia",
            "code": "TUN"
        },
        {
            "name": "Turkey",
            "code": "TUR"
        },
        {
            "name": "Turkmenistan",
            "code": "TKM"
        },
        {
            "name": "Uganda",
            "code": "UGA"
        },
        {
            "name": "Ukraine",
            "code": "UKR"
        },
        {
            "name": "United Arab Emirates",
            "code": "ARE"
        },
        {
            "name": "United Kingdom of Great Britain and Northern Ireland",
            "code": "GBR"
        },
        {
            "name": "United Republic of Tanzania",
            "code": "TZA"
        },
        {
            "name": "United States of America",
            "code": "USA"
        },
        {
            "name": "Uruguay",
            "code": "URY"
        },
        {
            "name": "Uzbekistan",
            "code": "UZB"
        },
        {
            "name": "Vanuatu",
            "code": "VUT"
        },
        {
            "name": "Venezuela (Bolivarian Republic of)",
            "code": "VEN"
        },
        {
            "name": "Viet Nam",
            "code": "VNM"
        },
        {
            "name": "Yemen",
            "code": "YEM"
        },
        {
            "name": "Zambia",
            "code": "ZMB"
        },
        {
            "name": "Zimbabwe",
            "code": "ZWE"
        }
    ];

    if(i<0 || i>=nameCode.length)
        return -1;
    else
        if(n===0)
            return nameCode[i].name;
        else if(n===1)
            return nameCode[i].code;
        else{
            for(let i=0;i<nameCode.length;i++){
                if( nameCode[i].name===c )
                    return nameCode[i].code
            }
        }
}


function DOMs() {
    let worldCountry = [ "Afghanistan",
        "Albania",
        "Algeria",
        "Angola",
        "Antigua and Barbuda",
        "Argentina",
        "Armenia",
        "Australia",
        "Austria",
        "Azerbaijan",
        "Bahamas",
        "Bahrain",
        "Bangladesh",
        "Barbados",
        "Belarus",
        "Belgium",
        "Belize",
        "Benin",
        "Bhutan",
        "Bolivia (Plurinational State of)",
        "Bosnia and Herzegovina",
        "Botswana",
        "Brazil",
        "Brunei Darussalam",
        "Bulgaria",
        "Burkina Faso",
        "Burundi",
        "Cabo Verde",
        "Cambodia",
        "Cameroon",
        "Canada",
        "Central African Republic",
        "Chad",
        "Chile",
        "China",
        "Colombia",
        "Comoros",
        "Congo",
        "Costa Rica",
        "Côte d'Ivoire",
        "Croatia",
        "Cuba",
        "Cyprus",
        "Czechia",
        "Democratic People's Republic of Korea",
        "Democratic Republic of the Congo",
        "Denmark",
        "Djibouti",
        "Dominican Republic",
        "Ecuador",
        "Egypt",
        "El Salvador",
        "Equatorial Guinea",
        "Eritrea",
        "Estonia",
        "Eswatini",
        "Ethiopia",
        "Fiji",
        "Finland",
        "France",
        "Gabon",
        "Gambia",
        "Georgia",
        "Germany",
        "Ghana",
        "Greece",
        "Grenada",
        "Guatemala",
        "Guinea",
        "Guinea-Bissau",
        "Guyana",
        "Haiti",
        "Honduras",
        "Hungary",
        "Iceland",
        "India",
        "Indonesia",
        "Iran (Islamic Republic of)",
        "Iraq",
        "Ireland",
        "Israel",
        "Italy",
        "Jamaica",
        "Japan",
        "Jordan",
        "Kazakhstan",
        "Kenya",
        "Kiribati",
        "Kuwait",
        "Kyrgyzstan",
        "Lao People's Democratic Republic",
        "Latvia",
        "Lebanon",
        "Lesotho",
        "Liberia",
        "Libya",
        "Lithuania",
        "Luxembourg",
        "Madagascar",
        "Malawi",
        "Malaysia",
        "Maldives",
        "Mali",
        "Malta",
        "Mauritania",
        "Mauritius",
        "Mexico",
        "Micronesia (Federated States of)",
        "Mongolia",
        "Montenegro",
        "Morocco",
        "Mozambique",
        "Myanmar",
        "Namibia",
        "Nepal",
        "Netherlands",
        "New Zealand",
        "Nicaragua",
        "Niger",
        "Nigeria",
        "Norway",
        "Oman",
        "Pakistan",
        "Panama",
        "Papua New Guinea",
        "Paraguay",
        "Peru",
        "Philippines",
        "Poland",
        "Portugal",
        "Qatar",
        "Republic of Korea",
        "Republic of Moldova",
        "Republic of North Macedonia",
        "Romania",
        "Russian Federation",
        "Rwanda",
        "Saint Lucia",
        "Saint Vincent and the Grenadines",
        "Samoa",
        "Sao Tome and Principe",
        "Saudi Arabia",
        "Senegal",
        "Serbia",
        "Seychelles",
        "Sierra Leone",
        "Singapore",
        "Slovakia",
        "Slovenia",
        "Solomon Islands",
        "Somalia",
        "South Africa",
        "South Sudan",
        "Spain",
        "Sri Lanka",
        "Sudan",
        "Suriname",
        "Sweden",
        "Switzerland",
        "Syrian Arab Republic",
        "Tajikistan",
        "Thailand",
        "Republic of North Macedonia",
        "Timor-Leste",
        "Togo",
        "Tonga",
        "Trinidad and Tobago",
        "Tunisia",
        "Turkey",
        "Turkmenistan",
        "Uganda",
        "Ukraine",
        "United Arab Emirates",
        "United Kingdom of Great Britain and Northern Ireland",
        "United Republic of Tanzania",
        "United States of America",
        "Uruguay",
        "Uzbekistan",
        "Vanuatu",
        "Venezuela (Bolivarian Republic of)",
        "Viet Nam",
        "Yemen",
        "Zambia",
        "Zimbabwe"
    ];

    let s = '<form>';

    for(let i=0; i<worldCountry.length; i++){
        s += '<option value="';
        s += worldCountry[i];
        s += '">';
        s += worldCountry[i];
        s += '</option>';
    }

    s += '</form>';

    document.getElementById("DOMin1").innerHTML += s;
    document.getElementById("DOMin2").innerHTML += s;
}